x = [i for i in range(0,100)]
y = []
for i in x:
    xdiv, xmod = divmod(i,10)
    if xdiv % 2 == 1:
        xmod = abs(xmod - 9)
    y.append((xdiv * 10) + xmod)
for i in range(0,10):
    print(y[i*10:(i+1)*10])
