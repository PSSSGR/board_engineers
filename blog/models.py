from django.db import models
from django.core.exceptions import ValidationError
# Create your models here.
def id_watcher(value):
    if value < 0:
        raise ValidationError(
            ('Location Id is %(value)s ? This can\'t be, value must be greater than 0'),
            params={'value': value},
        )

class Board_class(models.Model):
	# piece_id = models.PositiveIntegerField(validators=[id_watcher])
	# state = models.	models.BooleanField()
	# color = models.CharField(max_length = 20, default='white')
	piece_id = models.AutoField(validators=[id_watcher], primary_key=True)
	state = models.BooleanField()
	color = models.PositiveIntegerField(validators=[id_watcher], default = 0)
	label = models.CharField(max_length=20, default='null')
	image_identity = models.CharField(max_length=20, null=True, blank=True)


	# def __str__(self):
		# return self.color

	# def __repr__(self):
	# 	return "Id : " + str(self.piece_id) + " " + "Color : "+  self.color

class Character_Images(models.Model):
	image_id = models.AutoField(validators=[id_watcher], primary_key=True)
	character_image = models.ImageField(null = True, blank=True) 

