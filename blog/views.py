from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import *
from .forms import *
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
import json
from django.http import JsonResponse

# Create your views here.
@csrf_exempt
def home(request):
	return render(request, 'blog/home.html')


@csrf_exempt
def data(request):
	context = dict()
	if request.method == 'POST':
		for i in range(100): 
			square_piece = json.loads(dict(request.POST)['undefined'][i]);
			if "image_identity" in square_piece:
				db = Board_class(piece_id=square_piece["piece_id"], state = square_piece["triggered_state"] , 
					color=square_piece["env_color"], label = square_piece["character_info"], image_identity=square_piece["image_identity"])
				db.save()
			else:
				db = Board_class(piece_id=square_piece["piece_id"], state = square_piece["triggered_state"] , 
					color=square_piece["env_color"], label = square_piece["character_info"])
				# db.save()
				db.save(update_fields=["state", "color","label"]) 
		return HttpResponse("DB created")
	else:
		data = list(Board_class.objects.values())
		return JsonResponse(data, status=200, safe = False)

@csrf_exempt
def images(request):
	if request.method == 'POST': 
		# form = ImageForm(request.POST, request.FILES)
		# print("j")
		print(request.POST)
		print(request.FILES)
		image = request.FILES['image']
		db = Character_Images()
		db.character_image.save(image.name,image)
		# db.save(image)
		return HttpResponse('image upload success')	
		# if form.is_valid():
		# 	form.save()""
		# 	return JsonResponse({'error': False, 'message': 'Uploaded Successfully'})
		# else:
		# 	return JsonResponse({'error': True, 'errors': form.errors})
	else:
		print(Character_Images.objects)
		data = list(Character_Images.objects.values())
		return JsonResponse(data,  safe=False)
		# return HttpResponse('image get success')	
		# if form.is_valid():
		# 	print("Hello Worlds")
		# 	# m = ExampleModel.objects.get(pk=course_id)
		# 	m = Character_Images(image_id=1, character_image= form.cleaned_data['image'])
		# 	print("got here1")
		# 	# m.model_pic = form.cleaned_data['image']
		# 	m.save()
		# 	print("got here")
	
		# else:
		# data = list(Board_class.objects.values())
		# return JsonResponse(data, status=200, safe = False)

def success(request): 
    return HttpResponse('successfully uploaded')
