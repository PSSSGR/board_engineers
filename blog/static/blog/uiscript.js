var taken_tiles = [];
var change_in_progress = false;
var Image_data = null;
var previous_version = null;
var character_name = null;

$(document).ready(function(){
  console.log("Connected");
  get_images();
  var button_wrapper = document.getElementsByClassName("buttongrid");
  var myHTML = '';
  for (var i = 0; i < 100; i++){
      myHTML += '<button id="' + i + '"></button>';
  }
  button_wrapper[0].innerHTML = myHTML;

  $("a").click(function(){
  	if ($(this).attr("id") == "wall"){
      change_color($(this).attr("id"));
      post_data();
    }
    else if ($(this).attr("id") == "tree"){
      change_color($(this).attr("id"));
      post_data();
    }
    else if ($(this).attr("id") == "rock"){
      change_color($(this).attr("id"));
      post_data();
    }
    else if ($(this).attr("id") == "water"){
      change_color($(this).attr("id"));
      post_data();
    }
    else if ($(this).attr("id") == "fire"){
      change_color($(this).attr("id"));
      post_data();
    }
    else if ($(this).attr("id") == "deselect"){
      change_color($(this).attr("id"));
      post_data();
    }
  });
  // $("#final_submit").click(post_data);
  setInterval(get_data, 1000);
  $(".buttongrid").on('click', 'button', function (){
    console.log("From ready selected");
    $(this).toggleClass("selected");
    change_in_progress = check_selection();
  });
//This code is for testing, it adds in elements that are "triggered" meaning there would be pieces on them
//Uncomment below to use test code
  $("button").each(function(){
    console.log("button");
     if ($(this).attr("id")%13 == "6"){
        console.log("got it");
         $(this).addClass("triggered");
     }
  });
  // end of test code
  // ======Start of adding and listing pieces======
  var charactersNodelist = document.getElementsByTagName("LI");
  // console.log(charactersNodelist);
  var i;
  for (i = 0; i < charactersNodelist.length; i++) {
    var span = document.createElement("SPAN");
    var txt = document.createTextNode("\u00D7");
    span.className = "close";
    span.appendChild(txt);
    charactersNodelist[i].appendChild(span);
  }
  // Add a "checked" symbol when clicking on a list item
  var list = document.querySelector('ul');
  list.addEventListener('click', function(ev) {
    if (ev.target.tagName === 'LI') {
      ev.target.classList.toggle('checked');
    }
  }, false);


});

// function to change color of selected squares
function change_color(id){
  $("button").each(function() {
      var classes = "";
      if ($(this).hasClass("selected")){
          if (!($(this).hasClass(id))){
                classes = id;
          }
          if ($(this).hasClass("triggered")) {
              classes += " triggered";
          }
          $(this).attr("class", classes);
      }
 });
}

function get_data(){
  // event.preventDefault()
  console.log("Getting Data")
  if (change_in_progress == false){
    $.ajax({
        type: "GET",
        url: "/data",
        data: "",
        success: function (response) {
          var tile_changed = "";
          var changed_tile_number = null;
          var letters = ["A","B","C","D","E","F","G","H","I","J"];
          if (previous_version == null){
              previous_version = response;
          }else{
            for (var i = 0; i < 100; i++){
              if (previous_version[i]['state'] != response[i]['state'] && previous_version[i]['state']){
                character_name = previous_version[i]['label'];
                temp = previous_version[i]['label'];
                response[i]['label'] = '';
              }
            }
            for(var i = 0; i < 100; i++){
              if (previous_version[i]['state'] != response[i]['state'] && response[i]['state']){
                response[i]['label'] = character_name;
                character_name = '';
                changed_tile_number = i;
              }
            }
          }
          if (tile_changed != "" && changed_tile_number != null ){
            var character_list = document.getElementById("character_list").getElementsByTagName("li");
            for (var i =0; i < character_list.length;i++){
              console.log(character_list[i].childNodes[0]);
              id = (character_list[i]["textContent"]).split(", ");
              name = id[0];
              if (name == tile_changed){
                var character_string = name + ", " + letters[(changed_tile_number%10)] + (Math.floor(changed_tile_number/10)+1);
                var textnode = document.createTextNode(character_string);
                character_list[i].replaceChild(textnode, character_list[i].childNodes[0]);
              }
            }
          }
          var d = new Date();
          var t = d.toLocaleTimeString();
          console.log(t);
          var button_wrapper = document.getElementsByClassName("buttongrid");
          var myHTML = '';
          for (var i = 0; i < 100; i++) {
                  var class_list = "";
                  if (response[i]["color"] == 0){
                          class_list = "";
                  }else if(response[i]["color"] == 1){
                          class_list = "wall ";
                  }else if(response[i]["color"] == 2){
                          class_list = "tree ";
                  }else if(response[i]["color"] == 3){
                          class_list = "rock ";
                  }else if(response[i]["color"] == 4){
                          class_list = "water ";
                  }else if(response[i]["color"] == 5){
                          class_list = "fire ";
                  }

                  if(response[i]["state"]){
                          class_list += "triggered";
                  }
                  var image_name = "static/blog/" + response[i]["image_identity"];
                  myHTML += '<button id="' + i + '" class = "' + class_list +'" style="background-image: url('+ image_name+');"></button>';
          }
          button_wrapper[0].innerHTML = myHTML;
          console.log("Page update successful");
        }
      });
      return false;
  }

}
function post_data(){
  event.preventDefault()
  grid = [];
  character_data = [];
  var boxes = document.getElementsByClassName("buttongrid");
  var character_list = document.getElementById("character_list").getElementsByTagName("li");
  console.log(character_list.length)
  

  console.log(character_data);
  for(var i = 0; i  < 100; i++){
      var character_name = "";    
      var label = false;
      var square_color = 0;

      class_name = boxes[0]['children'][i]["className"];
      console.log(class_name);
      classes = class_name.split(" ");
      label = classes.includes("triggered");
      if (classes.includes("wall")){
        square_color = 1;
      }else if (classes.includes("tree")){
        square_color = 2;
      }else if (classes.includes("rock")){
        square_color = 3;
      }else if (classes.includes("water")){
        square_color = 4;
      }else if (classes.includes("fire")){
        square_color = 5;
      }
      // console.log(square_color);
      // console.log(label);
      // console.log(class_name);
      
      if (label){
        for(var j = 0; j < character_list.length; j++){
          // character_data.push((character_list[j]["textContent"]).split(", "));
          id = (character_list[j]["textContent"]).split(", ");
          number = get_square_number_by_id(id[1]);
          if (number == i) {
            character_name = id[0];
          }
        }
      }

    grid.push({
      key: i ,
      value: JSON.stringify({
      piece_id: i,
      env_color: square_color,
      triggered_state: label,
      character_info: character_name
      })});
    }
  console.log(grid[1]);
  $.ajax({
      type: "POST",
      url: "/data",
      data: grid,
      success: function () {
        console.log("POST call successful");
        change_in_progress = false;
      }
    });
  return false;
}

function get_images(){
  console.log("Getting images")
  $.ajax({
      type: "GET",
      url: "/images",
      data: "",
      success: function (response) {
        Image_data = response;
        var image_wrapper = document.getElementById("image_picker");
        var myHTML = '';
        for (var i = 0; i < response.length; i++){
          console.log(response[i]["character_image"]);
          myHTML +=  '<img src="static/blog/'+response[i]["character_image"] + '" height="50" width="50" onclick="image_click(this.src)">' ;
        }
        console.log(myHTML);
        image_wrapper.innerHTML = myHTML;
      }
    });
    return false;
}

function openModal(){
  var modal = document.getElementById("myModal");
  var btn = document.getElementById("myBtn");
  var span = document.getElementsByClassName("close_modal")[0];
  btn.onclick = function() {
    modal.style.display = "block";
  }
  span.onclick = function() {
    modal.style.display = "none";
  }
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  }

}

function image_click(image_src){
  image_name = image_src.split("/")
  image_name = image_name[image_name.length-1];
  triggered_tiles = []
  var triggered_tile_id = null;
  $("button").each(function() {
    if ($(this).hasClass("selected")){
        if ($(this).hasClass("triggered")) {
          triggered_tiles.push($(this).attr("id"));
          triggered_tile_id = $(this).attr("id");
        }
    }
  });
  number_of_triggered_tiles = triggered_tiles.length;
  if (number_of_triggered_tiles === 0) {
    alert("Please select a triggered square and then retry.");
  }
  else if(number_of_triggered_tiles > 1){
    alert("Please only select 1 triggered square.")
  }
  else{
    grid = [];
    character_data = [];
    var boxes = document.getElementsByClassName("buttongrid");
    console.log(boxes);
    var character_list = document.getElementById("character_list").getElementsByTagName("li");
    // var character_list = []
    for(var i = 0; i  < 100; i++){
        var character_name = "";    
        var label = false;
        var square_color = 0;
         class_name = boxes[0]['children'][i]["className"];
        console.log(class_name);
        classes = class_name.split(" ");
        label = classes.includes("triggered");
        if (classes.includes("wall")){
          square_color = 1;
        }else if (classes.includes("tree")){
          square_color = 2;
        }else if (classes.includes("rock")){
          square_color = 3;
        }else if (classes.includes("water")){
          square_color = 4;
        }else if (classes.includes("fire")){
          square_color = 5;
        }
        
        if (label){
          for(var j = 0; j < character_list.length; j++){
            // character_data.push((character_list[j]["textContent"]).split(", "));
            id = (character_list[j]["textContent"]).split(", ");
            number = get_square_number_by_id(id[1]);
            if (number == i) {
              character_name = id[0];
            }
          }
        }
        image_identifier = null;
        if (triggered_tile_id == i){
          image_identifier = image_name;
          grid.push({
          key: i ,
          value: JSON.stringify({
          piece_id: i,
          env_color: square_color,
          triggered_state: label,
          character_info: character_name,
          image_identity: image_identifier 
          })});
        }else{
          grid.push({
            key: i ,
            value: JSON.stringify({
            piece_id: i,
            env_color: square_color,
            triggered_state: label,
            character_info: character_name
            })});
        }
      }
    $.ajax({
        type: "POST",
        url: "/data",
        data: grid,
        success: function () {
          console.log("POST call successful");
          change_in_progress = false;
        }
      });
    return false;
  }
}

// Create a new list item when clicking on the "Add" button
function newElement() {
  var li = document.createElement("li");
  var inputValue = document.getElementById("characterInput").value;
  var letters = ["A","B","C","D","E","F","G","H","I","J"];
  var tile_list = [];
 
  var character_added = false;

  $("button").each(function() {
        if ($(this).hasClass("selected")){
            if ($(this).hasClass("triggered")) {
                tile_list.push(letters[($(this).attr("id")%10)] + (Math.floor($(this).attr("id")/10)+1));

            }
        }
   });

  var character_string = inputValue;

  for(var i = 0; i < tile_list.length; i++) {
    character_string += ", " + tile_list[i];
  }

  var t = document.createTextNode(character_string);
  li.appendChild(t);

  if (inputValue === '') {
    alert("You must input a name.");
  }
  // else if (tile_list.length == 0) {
  //   alert("You must select a piece to add.");
  // }
  else if(tile_list.length > 1){
    alert("Please only select 1 element.")
  }
  // else if(taken_tiles.includes(tile_list[0])){
  //   alert("Tile already taken.");
  // }
  else {
    document.getElementById("character_list").appendChild(li);
    character_added = true;
    taken_tiles.push(tile_list[0]);
  }

  if(character_added){
    document.getElementById("characterInput").value = "";
  }

  var span = document.createElement("SPAN");
  var txt = document.createTextNode("\u00D7");
  span.className = "close";
  span.appendChild(txt);
  li.appendChild(span);

  for (var i = 0; i < close.length; i++) {
    close[i].onclick = function() {
      var div = this.parentElement;
      div.style.display = "none";
      var location = "";
      if (this.parentElement.textContent.slice(-2,-1) == "0"){
        location = this.parentElement.textContent.slice(-4,-1);
      }
      else{
        location = this.parentElement.textContent.slice(-3,-1);
      }
      for (var i = 0; i < taken_tiles.length; i++) {
        if (taken_tiles[i] == location) {
            taken_tiles.splice(i,1)
        }
      }
    }
  }
};
// Click on a close button to hide the current list item
var close = document.getElementsByClassName("close");
var i;
for (i = 0; i < close.length; i++) {
  close[i].onclick = function() {
    var div = this.parentElement;
    div.style.display = "none";
    var location = "";
    if (this.parentElement.textContent.slice(-2,-1) == "0"){
        location = this.parentElement.textContent.slice(-4,-1);
    }
    else{
        location = this.parentElement.textContent.slice(-3,-1);
    }
    for (var i = 0; i < taken_tiles.length; i++) {
        if (taken_tiles[i] == location) {
            taken_tiles.splice(i,1)
        }
    }
  }
};

function get_square_number_by_id(square_name){
  square_name = square_name.split("");
  var letters = ["A","B","C","D","E","F","G","H","I","J"];
  var row = Number(square_name[1] - 1) * 10;
  var column = letters.indexOf(square_name[0]);
  return (row + column);
}

function check_selection(){
  selection_in_progress = false;
  $("button").each(function() {
    if ($(this).hasClass("selected")){
      selection_in_progress = true;
    }
  });
  return selection_in_progress;
}
// ======End of adding and listing pieces======