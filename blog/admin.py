from django.contrib import admin
from .models import Board_class, Character_Images
# Register your models here.

admin.site.register(Board_class)
admin.site.register(Character_Images)